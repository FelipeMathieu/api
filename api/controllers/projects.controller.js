const projectsService = require('../../services/projects.service');

class ProjectsController {
    getProject = (req, res) => {
        try {
            const id = req.params.id;
            return res.status(200).json(projectsService.getById(id));
        } catch (err) {
            res.status(500).send({
                status: 500,
                message: err.message
            });
        }
    }

    getUserProjects = (req, res) => {
        try {
            const id = req.params.id;
            const result = projectsService.getUserProjects(id);
            return res.status(200).json(result);
        } catch (err) {
            res.status(500).send({
                status: 500,
                message: err.message
            });
        }
    }

    createProject = (req, res) => {
        try {
            const data = { ...req.body };
            const result = projectsService.createProject(data);
            return res.status(200).json(result);
        } catch (err) {
            res.status(500).send({
                status: 500,
                error: 'Fail to create new project',
                message: err.message
            });
        }
    }

    updateProject = (req, res) => {
        try {
            const data = { ...req.body };
            const result = projectsService.updateProject(data);
            return res.status(200).json(result);
        } catch (err) {
            res.status(500).send({
                status: 500,
                error: 'Fail to create new project',
                message: err.message
            });
        }
    }

    delete = (req, res) => {
        try {
            const id = req.params.id;
            const result = projectsService.delete(id);
            if(!!result) {
                return res.status(200).send(id);
            } else {
                throw new Error('Fail to delete');
            }
        } catch (err) {
            res.status(500).send({
                status: 500,
                error: 'Fail to delete project',
                message: err.message
            });
        }
    }
}

module.exports = new ProjectsController();