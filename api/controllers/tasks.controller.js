const tasksService = require('../../services/tasks.service');

class TasksController {
    getTask = (req, res) => {
        try {
            const id = req.params.id;
            return res.status(200).json(tasksService.getById(id));
        } catch (err) {
            res.status(500).send({
                status: 500,
                message: err.message
            });
        }
    }

    getProjectTasks = (req, res) => {
        try {
            const id = req.params.id;
            const result = tasksService.getProjectTasks(id);
            return res.status(200).json(result);
        } catch (err) {
            res.status(500).send({
                status: 500,
                message: err.message
            });
        }
    }

    createTask = (req, res) => {
        try {
            const data = { ...req.body };
            const result = tasksService.createTask(data);
            return res.status(200).json(result);
        } catch (err) {
            res.status(500).send({
                status: 500,
                error: 'Fail to create new task',
                message: err.message
            });
        }
    }

    updateTask = (req, res) => {
        try {
            const data = { ...req.body };
            const result = tasksService.updateTask(data);
            return res.status(200).json(result);
        } catch (err) {
            res.status(500).send({
                status: 500,
                error: 'Fail to update task',
                message: err.message
            });
        }
    }

    close = (req, res) => {
        try {
            const id = req.params.id;
            const result = tasksService.close(id);
            return res.status(200).json(result);
        } catch (err) {
            res.status(500).send({
                status: 500,
                error: 'Fail to close task',
                message: err.message
            });
        }
    }

    delete = (req, res) => {
        try {
            const id = req.params.id;
            const result = tasksService.delete(id);
            if(!!result) {
                return res.status(200).send(id);
            } else {
                throw new Error('Fail to delete');
            }
        } catch (err) {
            res.status(500).send({
                status: 500,
                error: 'Fail to delete task',
                message: err.message
            });
        }
    }
}

module.exports = new TasksController();