const usersService = require('../../services/users.service');

class UsersController {
    login = async(req, res) => {
        try {
            const result = await usersService.login(req.body);
            return res.status(200).json(result);
        } catch (err) {
            res.status(500).send({
                status: 500,
                message: err.message
            });
        }
    }

    getUserByEmail = (req, res) => {
        try {
            const email = req.params.email;
            const result = usersService.getByEmail(email);
            if(!result) {
                return res.status(404).send({
                    status: 404,
                    message: 'User not found'
                });
            }
            return res.status(200).json(result);
        } catch (err) {
            res.status(500).send({
                status: 500,
                message: err.message
            });
        }
    }

    createUser = (req, res) => {
        try {
            const data = { ...req.body };
            const result = usersService.createUser(data);
            return res.status(200).json(result);
        } catch (err) {
            res.status(500).send({
                status: 500,
                error: 'Fail to create new user',
                message: err.message
            });
        }
    }
}

module.exports = new UsersController();