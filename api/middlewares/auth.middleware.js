class Auth {
    check = (req, res, done) => {
        const user = req.params.token;
        if(user.id) {
            done();
        }
        res.status(401).send('User unauthorized');
    }
}

module.exports = new Auth();