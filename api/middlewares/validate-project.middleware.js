const validator = require('../../helpers/validators/validator');
const projectRule = require('../../helpers/validators/rules/project.rule');

const ValidateProject = (req, res, next) => {
    validator(req.body, projectRule, {}, (err, status) => {
        if(!status && !!err) {
            res.status(412)
                .send({
                    success: false,
                    message: 'Validation failed',
                    data: err
                });
        } else {
            next();
        }
    });
}

module.exports = {
    ValidateProject
}