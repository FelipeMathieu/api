const validator = require('../../helpers/validators/validator');
const taskRule = require('../../helpers/validators/rules/task.rule');

const ValidateTask = (req, res, next) => {
    validator(req.body, taskRule, {}, (err, status) => {
        if(!status && !!err) {
            res.status(412)
                .send({
                    success: false,
                    message: 'Validation failed',
                    data: err
                });
        } else {
            next();
        }
    });
}

module.exports = {
    ValidateTask
}