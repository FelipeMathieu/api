const validator = require('../../helpers/validators/validator');
const userRule = require('../../helpers/validators/rules/user.rule');

const ValidateUser = (req, res, next) => {
    validator(req.body, userRule, {}, (err, status) => {
        if(!status && !!err) {
            res.status(412)
                .send({
                    success: false,
                    message: 'Validation failed',
                    data: err
                });
        } else {
            next();
        }
    });
}

module.exports = {
    ValidateUser
}