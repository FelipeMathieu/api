const controller = require('../controllers/projects.controller');
const validation = require('../middlewares/validate-project.middleware');

const baseUrl = '/api/projects';

module.exports = app => {
    app.route(`${baseUrl}/:id`)
        .get(controller.getProject);

    app.route(`/api/users/:id/projects`)
        .get(controller.getUserProjects);

    app.route(`${baseUrl}`)
        .post(validation.ValidateProject, controller.createProject);

    app.route(`${baseUrl}`)
        .put(controller.updateProject);

    app.route(`${baseUrl}/:id/delete`)
        .put(controller.delete);
}