const controller = require('../controllers/tasks.controller');
const validation = require('../middlewares/validate-task.middleware');

const baseUrl = '/api/tasks';

module.exports = app => {
    app.route(`${baseUrl}/:id`)
        .get(controller.getTask);

    app.route(`/api/projects/:id/tasks`)
        .get(controller.getProjectTasks);

    app.route(`${baseUrl}`)
        .post(validation.ValidateTask, controller.createTask);

    app.route(`${baseUrl}/:id/close`)
        .put(controller.close);

    app.route(`${baseUrl}/:id/delete`)
        .put(controller.delete);
}