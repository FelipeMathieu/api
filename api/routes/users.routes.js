const controller = require('../controllers/users.controller');
const validation = require('../middlewares/validate-user.middleware');

const baseUrl = '/api/users';

module.exports = app => {
    app.route(`${baseUrl}/:email`)
        .get(controller.getUserByEmail);

    app.route(`${baseUrl}/login`)
        .post(validation.ValidateUser, controller.login);
    
    app.route(`${baseUrl}`)
        .post(validation.ValidateUser, controller.createUser);
}