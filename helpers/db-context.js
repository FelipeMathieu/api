const loki = require('lokijs');

const minute = 1000 * 60;
const instance = new loki('../db/EDirectDB.db', {
    autoload: true,
    autosave: true,
    autosaveInterval: minute,
});
const collections = {
    Users: instance.addCollection('users'),
    Projects: instance.addCollection('projects'),
    Tasks: instance.addCollection('tasks')
};

module.exports = {
    instance,
    collections,
};