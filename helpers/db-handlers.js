const db = require('./db-context');

class DbHandlers {
    static load = (id, collectionName) => {
        const result = db.collections[collectionName].get(+id);
        if(!result) {
            throw new Error(`${collectionName} does not exist`);
        }
        return result;
    }

    static loadAll = (collectionName) => {
        const result = db.collections[collectionName].find({ '$loki': { '$ne': null } });
        if(!result) {
            throw new Error(`${collectionName} does not exist`);
        }
        return result;
    }
}

module.exports = DbHandlers;