class ObjectKeys {
    static properties = (data) => {
        return Object.keys(data).filter(key => !!typeof data[key] && typeof data[key] !== 'function');
    }
}

module.exports = ObjectKeys;