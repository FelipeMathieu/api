const DataObjectRule = {
    id: 'integer',
    createdDate: 'date',
    endDate: 'date',
    isDeleted: 'boolean'
}

module.exports = DataObjectRule;