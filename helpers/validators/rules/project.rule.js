const DataObjectRule = require("./data-object.rule");

const ProjectRule = {
    ...DataObjectRule,
    creatorId: 'required|min:1',
    name: 'required|string',
};

module.exports = ProjectRule;