const DataObjectRule = require("./data-object.rule");

const TaskRule = {
    ...DataObjectRule,
    description: 'required|string',
    projectId: 'required|min:1'
}

module.exports = TaskRule;