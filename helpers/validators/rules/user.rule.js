const DataObjectRule = require("./data-object.rule");

const UserRule = {
    ...DataObjectRule,
    name: 'string',
    email: 'required|email',
    password: 'required|string',
}

module.exports = UserRule;