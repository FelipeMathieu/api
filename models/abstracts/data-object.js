class DataObject {
    id = 0;
    createdDate = new Date(Date.now());
    endDate = null;
    isDeleted = false;
}

module.exports = DataObject;