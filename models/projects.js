const DataObject = require("./abstracts/data-object");
const ObjectKeys = require("../helpers/object-keys");
const db = require('../helpers/db-context');
const DbHandlers = require("../helpers/db-handlers");

class Projects extends DataObject {
    #row = null;
    creatorId = null;
    name = '';

    constructor(data) {
        super();
        if(!!data) {
            this.#mapFields(ObjectKeys.properties(this), data);
            this.id = this.$loki;
            this.#row = data;
        }
    }

    static load = (id) => {
        const result = DbHandlers.load(id, 'Projects');
        return Projects.getMappedResult(result);
    }

    static loadAllById = (id) => {
        const result = db.collections.Projects.find({
            'creatorId': { '$eq': +id },
            'isDeleted': { '$eq': false }
        });
        if(!result) {
            throw new Error(`Projects does not exist`);
        }
        return result.map(item => Projects.getMappedResult(item));
    }

    static getMappedResult = (item) => {
        return {
            ...new Projects(item),
            id: item.$loki
        };
    }

    fill = (data) => {
        this.#mapFields(ObjectKeys.properties(this), data);
    }

    save = () => {
        this.#row === null ? this.create() : this.update();
        this.id = this.#row.$loki;
    }

    create = () => {
        const keys = ObjectKeys.properties(this);
        let result = {};
        keys.map(key => key !== 'id' ? result[key] = this[key] : null);
        this.#checkIfProjectExists(result.creatorId, result.name);
        this.#row = db.collections.Projects.insert(result);
    }

    update = (data) => {
        const keys = ObjectKeys.properties(data);
        keys.map(key => key !== 'id' ? this.#row[key] = this[key] : null);
        db.collections.Projects.update(this.#row);
        return Projects.load(this.id);
    }

    delete = (id) => {
        const keys = ObjectKeys.properties(this);
        this.isDeleted = true;
        keys.map(key => key !== 'id' ? this.#row[key] = this[key] : null);
        db.collections.Projects.update(this.#row);
        return Projects.load(id);
    }

    #mapFields = (keys, data) => {
        keys.map(key => this[key] = data[key] ?? this[key]);
    }

    #checkIfProjectExists = (userId, projectName) => {
        const projects = db.collections.Projects.find({
            'name': { '$eq': projectName },
            'creatorId': { '$eq': +userId },
            'isDeleted': { '$eq': false }
        });
        if(projects.length > 0) {
            throw new Error('You alreay have this project');
        }
    } 
}

module.exports = Projects;