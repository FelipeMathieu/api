const DataObject = require("./abstracts/data-object");
const ObjectKeys = require("../helpers/object-keys");
const db = require('../helpers/db-context');
const DbHandlers = require("../helpers/db-handlers");

class Tasks extends DataObject {
    #row = null;
    description = '';
    projectId = null;

    constructor(data) {
        super();
        if(!!data) {
            this.#mapFields(ObjectKeys.properties(this), data);
            this.id = this.$loki;
            this.#row = data;
        }
    }

    static load = (id) => {
        const result = DbHandlers.load(id, 'Tasks');
        return Tasks.getMappedResult(result);
    }

    static loadAllById = (id) => {
        const result = db.collections.Tasks.find({
            'projectId': { '$eq': +id },
            'isDeleted': { '$eq': false }
        });
        if(!result) {
            throw new Error(`Tasks does not exist`);
        }
        return result.map(item => Tasks.getMappedResult(item));
    }

    static getMappedResult = (item) => {
        return {
            ...new Tasks(item),
            id: item.$loki
        };
    }

    fill = (data) => {
        this.#mapFields(ObjectKeys.properties(this), data);
    }

    save = () => {
        this.#row === null ? this.create() : this.update();
        this.id = this.#row.$loki;
    }

    create = () => {
        const keys = ObjectKeys.properties(this);
        let result = {};
        keys.map(key => key !== 'id' ? result[key] = this[key] : null);
        this.#row = db.collections.Tasks.insert(result);
    }

    update = () => {
        const keys = ObjectKeys.properties(this);
        keys.map(key => key !== 'id' ? this.#row[key] = this[key] : null);
        db.collections.Tasks.update(this.#row);
    }

    close = (id) => {
        const keys = ObjectKeys.properties(this);
        this.endDate = new Date(Date.now());
        keys.map(key => key !== 'id' ? this.#row[key] = this[key] : null);
        db.collections.Tasks.update(this.#row);
        return Tasks.load(id);
    }

    delete = (id) => {
        const keys = ObjectKeys.properties(this);
        this.isDeleted = true;
        keys.map(key => key !== 'id' ? this.#row[key] = this[key] : null);
        db.collections.Tasks.update(this.#row);
        return Tasks.load(id);
    }

    #mapFields = (keys, data) => {
        keys.map(key => this[key] = data[key] ?? this[key]);
    }
}

module.exports = Tasks;