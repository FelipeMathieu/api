const DataObject = require("./abstracts/data-object");
const ObjectKeys = require("../helpers/object-keys");
const db = require('../helpers/db-context');
const bcrypt = require('bcrypt');
const saltRounds = 10;

class Users extends DataObject {
    #row = null;
    name = '';
    email = '';
    password = '';

    constructor(data) {
        super();
        if(!!data) {
            this.#mapFields(ObjectKeys.properties(this), data);
            this.id = this.$loki;
            this.#row = data;
        }
    }

    static load = (email) => {
        const result = db.collections.Users.find({'email': { '$eq' : email }});
        return result.length > 0 ? Users.getMappedResult(result[0]) : null;
    }

    static getMappedResult = (item) => {
        return {
            ...new Users(item),
            id: item.$loki
        };
    }

    static login = async(data) => {
        const result = Users.load(data.email);
        if(!result?.email) throw new Error('Invalid email');
        const match = await bcrypt.compare(data.password, result.password);
        if(!match) throw new Error('Invalid password');
        return result;
    }

    static getPasswordHash = (password) => {
        return bcrypt.hashSync(password, saltRounds);
    }

    fill = (data) => {
        this.#mapFields(ObjectKeys.properties(this), data);
    }

    save = () => {
        this.#row === null ? this.create() : this.update();
        this.id = this.#row.$loki;
    }

    create = () => {
        const keys = ObjectKeys.properties(this).filter(key => key !== 'password');
        let result = {};
        keys.map(key => key !== 'id' ? result[key] = this[key] : null);
        this.#checkIfUserExists();
        result.password = Users.getPasswordHash(this.password);
        this.#row = db.collections.Users.insert(result);
    }

    update = () => {
        const keys = ObjectKeys.properties(this).filter(key => key !== 'password');
        keys.map(key => key !== 'id' ? this.#row[key] = this[key] : null);
        db.collections.Users.update(this.#row);
    }

    #mapFields = (keys, data) => {
        keys.map(key => this[key] = data[key] ?? this[key]);
    }

    #checkIfUserExists = () => {
        const result = Users.load(this.email);
        if(!!result?.email) {
            throw new Error('User already exists');
        }
    }
}

module.exports = Users;