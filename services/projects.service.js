const Projects = require('../models/projects');

class ProjectsService {
    getById = (id) => {
        return Projects.load(id);
    }

    getUserProjects = (id) => {
        return Projects.loadAllById(id);
    }

    createProject = (data) => {
        const result = new Projects();
        result.fill(data);
        result.save();
        return result;
    }

    updateProject = (data) => {
        const result = Projects.load(data.id);
        result.fill(data);
        return result.update(data);
    }

    delete = (id) => {
        const result = Projects.load(id);
        return result.delete(id);
    }
}

module.exports = new ProjectsService();