const Tasks = require('../models/tasks');

class TasksService {
    getById = (id) => {
        return Tasks.load(id);
    }

    getProjectTasks = (id) => {
        return Tasks.loadAllById(id);
    }

    createTask = (data) => {
        const result = new Tasks();
        result.fill(data);
        result.save();
        return result;
    }

    updateTask = (data) => {
        const result = Tasks.load(data.id);
        result.fill(data);
        result.save();
        return result;
    }

    close = (id) => {
        const result = Tasks.load(id);
        return result.close(id);
    }

    delete = (id) => {
        const result = Tasks.load(id);
        return result.delete(id);
    }
}

module.exports = new TasksService();