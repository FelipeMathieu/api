const Users = require('../models/users');

class UsersService {
    login = async(data) => {
        const result = Users.login(data);
        const response = await this.#removePasswordFromResult(result);
        return this.#removePasswordFromResult(response);
    }

    getByEmail = (email) => {
        const result = Users.load(email);
        return this.#removePasswordFromResult(result);
    }

    createUser = (data) => {
        const result = new Users();
        result.fill(data);
        result.save();
        return this.#removePasswordFromResult(result);
    }

    updateUser = (data) => {
        const result = Users.load(data.id);
        result.fill(data);
        result.save();
        return this.#removePasswordFromResult(result);
    }

    #removePasswordFromResult = (result) => {
        delete result['password'];
        return result;
    }
}

module.exports = new UsersService();